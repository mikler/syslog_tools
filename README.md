# syslog_tools

## Getting started

Syslog tools conatins two utilities for Glaber monitoring project:
  -  udp_replicator - a program for udp traffic replication, used to tee a stream to several streams (up to 64) of udp traffic. Useful for syslog replication, but might be used for any UDP data streams
  -  glb_syslog_worker - a Glaber worker for listening of Syslog rfc5425 and rfc3164 messages, converting and passing them to JSON

## Building
<code>
make udp_replicator  
make glb_syslog_worker  
</code>

## Usage 
<code>
udp_replicator  --listen=0.0.0.0:514 --forward=8.8.8.11 --forward=8.8.8.11:513 --forward=:517
glb_syslog_worker --listen=0.0.0.0:514
</code>


default ip address is 127.0.0.1, port is 514
In any of [listen, forward] in ip:port pairs any part might be ommited 

Paramters also might be passed through the environment varaibles:

<code>
export LISTEN=0.0.0.0:514
export FORWARD="127.0.0.1:515;127.0.0.1:516;:518"
./udp_replicator
</code>

## Notes

Performance: for high-loaded systems it's advised to increase incoming buffers for both utilites. For udp_replicator outgoing buffers might be increased as well to avoid traffic loss.

For linux systems consider using following sysctl parameters
<code>
net.core.rmem_max=128214400
net.core.rmem_default=128214400
</code>


udp_replicator uses approximately 10% of a modern CPU per stream of 10kps (10 thousand datagrams per second). Both for ingress and egress. So to handle copying one 10kps stream to 3 destinations it will need about 40% of a CPU (10% for ingress and 30% for egress).
Udp replicator is signle threaded, so make sure not to hit CPU core limit.

glb_syslog_worker needs about 15% of a CPU Core to handle 10kps syslog stream. So healthy limit to process is about 50kps messages per one instance.


