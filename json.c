/*MIT License

Copyright (c) 2013-2022 Niels Lohmann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// borrowed from
// https://github.com/nlohmann/json/blob/ec7a1d834773f9fee90d8ae908a0c9933c5646fc/src/json.hpp#L4604-L4697

#define MAX_SYSLOG_MESSAGE_SIZE 4096
#include <string.h>
#include <stdio.h>

char *escape_string_json(const char *msg)
{

    static char result[MAX_SYSLOG_MESSAGE_SIZE * 5];
    unsigned char c;
    int pos = 0;
    int m_pos = 0, len = strlen(msg);

    // create a result string of necessary size
    memset(result, '\\', MAX_SYSLOG_MESSAGE_SIZE * 5);

    while (m_pos < len)
    {
        c = msg[m_pos];

        switch (c)
        {
        // quotation mark (0x22)
        case '"':
        {
            result[pos + 1] = '"';
            pos += 2;
            break;
        }

        // reverse solidus (0x5c)
        case '\\':
        {
            // nothing to change
            pos += 2;
            break;
        }

        // backspace (0x08)
        case '\b':
        {
            result[pos + 1] = 'b';
            pos += 2;
            break;
        }

        // formfeed (0x0c)
        case '\f':
        {
            result[pos + 1] = 'f';
            pos += 2;
            break;
        }

        // newline (0x0a)
        case '\n':
        {
            result[pos + 1] = 'n';
            pos += 2;
            break;
        }

        // carriage return (0x0d)
        case '\r':
        {
            result[pos + 1] = 'r';
            pos += 2;
            break;
        }

        // horizontal tab (0x09)
        case '\t':
        {
            result[pos + 1] = 't';
            pos += 2;
            break;
        }

        default:
        {
            if (c >= 0x01 && c <= 0x1f)
            {
                // print character c as \uxxxx
                sprintf(&result[pos + 1], "u%04x", (int)c);
                pos += 6;
                // overwrite trailing null character
                result[pos] = '\\';
            }
            else
            {
                // all other characters are added as-is
                result[pos++] = c;
            }
            break;
        }
    }
    m_pos++;
    }
    result[pos] = '\0';
    return result;
}