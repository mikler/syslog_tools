glb_syslog_worker: glb_syslog_worker.c parser.c json.c
	gcc -o glb_syslog_worker glb_syslog_worker.c parser.c params.c socket.c json.c

glb_syslog_worker_go:
	go get gopkg.in/mcuadros/go-syslog.v2
	go build glb_syslog_worker.go
udp_replicator: 
	gcc -o udp_replicator udp_replicator.c params.c socket.c

clean:
	rm glb_syslog_worker udp_replicator

