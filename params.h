/* * Header file for the parser object
 *
 * File begun on 2007-08-06 by RGerhards (extracted from syslogd.c, which
 * was under BSD license at the time of rsyslog fork)
 *
 * Copyright 2007-2018 Adiscon GmbH.
 *
 * This file is formed on part of rsyslog suite.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//void parse_ip_port(struct sockaddr_in *server, char *option);
#include "common.h"

int init_params_env(settings_t *settings);
int init_params(settings_t *settings, int argc, char **argv);
int init_params_listen(settings_t *settings, int argc, char **argv);