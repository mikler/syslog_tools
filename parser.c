/* * Header file for the parser object
 *
 * File begun on 2007-08-06 by RGerhards (extracted from syslogd.c, which
 * was under BSD license at the time of rsyslog fork)
 *
 * Copyright 2007-2018 Adiscon GmbH.
 *
 * This file is formed on part of rsyslog suite.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 #define _XOPEN_SOURCE 
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <time.h>
#include <strings.h>

#include "parser.h"
#include "json.h"

#define	RFC3164_DATELEN	15
#define	RFC3164_DATEFMT	"%b %e %H:%M:%S"

#define logit printf

typedef struct _code {
	const char	*c_name;
	int	c_val;
} CODE;

CODE facilitynames[] = {
	{ "auth",	LOG_AUTH },
	{ "authpriv",	LOG_AUTHPRIV },
	{ "console",	LOG_CONSOLE },
	{ "cron",	LOG_CRON },
	{ "cron_sol",	LOG_CRON_SOL },		/* Solaris cron */
	{ "daemon",	LOG_DAEMON },
	{ "ftp",	LOG_FTP },
	{ "kern",	LOG_KERN },
	{ "lpr",	LOG_LPR },
	{ "mail",	LOG_MAIL },
	{ "mark",	INTERNAL_MARK },	/* INTERNAL */
	{ "news",	LOG_NEWS },
	{ "ntp",	LOG_NTP },
	{ "security",	LOG_SECURITY },
	{ "syslog",	LOG_SYSLOG },
	{ "user",	LOG_USER },
	{ "uucp",	LOG_UUCP },
	{ "local0",	LOG_LOCAL0 },
	{ "local1",	LOG_LOCAL1 },
	{ "local2",	LOG_LOCAL2 },
	{ "local3",	LOG_LOCAL3 },
	{ "local4",	LOG_LOCAL4 },
	{ "local5",	LOG_LOCAL5 },
	{ "local6",	LOG_LOCAL6 },
	{ "local7",	LOG_LOCAL7 },
	{ NULL,		-1 }
};

CODE prioritynames[] = {
	{ "alert",	LOG_ALERT },
	{ "crit",	LOG_CRIT },
	{ "debug",	LOG_DEBUG },
	{ "emerg",	LOG_EMERG },
	{ "err",	LOG_ERR },
	{ "error",	LOG_ERR },		/* DEPRECATED */
	{ "info",	LOG_INFO },
	{ "none",	INTERNAL_NOPRI },	/* INTERNAL */
	{ "notice",	LOG_NOTICE },
	{ "panic",	LOG_EMERG },		/* DEPRECATED */
	{ "warn",	LOG_WARNING },		/* DEPRECATED */
	{ "warning",	LOG_WARNING },
	{ "*",		INTERNAL_ALLPRI },	/* INTERNAL */
	{ NULL,		-1 }
};


static void
parsemsg_remove_unsafe_characters(const char *in, char *out, size_t outlen)
{
	char *q;
	int c;
	int mask_C1 = 1;

	q = out;
	while ((c = (unsigned char)*in++) != '\0' && q < out + outlen - 4) {
		if (mask_C1 && (c & 0x80) && c < 0xA0) {
			c &= 0x7F;
			*q++ = 'M';
			*q++ = '-';
		}
		if (isascii(c) && iscntrl(c)) {
			if (c == '\n') {
				*q++ = ' ';
			} else if (c == '\t') {
				*q++ = '\t';
			} else {
				*q++ = '^';
				*q++ = c ^ 0100;
			}
		} else {
			*q++ = c;
		}
	}
	*q = '\0';
}


/*
 * Decode a priority into textual information like auth.emerg.
 */
char *textpri(int pri)
{
	static char res[20];
	CODE *c_pri, *c_fac;

	for (c_fac = facilitynames; c_fac->c_name && !(c_fac->c_val == LOG_FAC(pri) << 3); c_fac++)
		;
	for (c_pri = prioritynames; c_pri->c_name && !(c_pri->c_val == LOG_PRI(pri)); c_pri++)
		;

	snprintf(res, sizeof(res), "%s.%s<%d>", c_fac->c_name, c_pri->c_name, pri);

	return res;
}

static char* sd_to_json(struct buf_msg *buf) {
    int i, len = 0;
    static char sd_buffer[4096];

    if (0 == buf->sd_count) 
        return "{}";
    
    len += snprintf(sd_buffer + len, 4096-len, "{");
    for (int i = 0; i < buf->sd_count; i++) {
        char sd_name[64], sd_value[64];
        bzero(sd_name,sizeof(sd_name));
        bzero(sd_value,sizeof(sd_value));

        strncpy(sd_name, buf->sd_pairs[i].name, buf->sd_pairs[i].name_len);
    
        if (buf->sd_pairs[i].value_len > 0)
            strncpy(sd_value, buf->sd_pairs[i].value, buf->sd_pairs[i].value_len);
        else 
            strcpy(sd_value, "nil");
        if (i > 0)
            len += snprintf(sd_buffer + len, 4096-len, ",");

        len += snprintf(sd_buffer + len, 4096-len, "\"%s\":", escape_string_json(sd_name));
        len += snprintf(sd_buffer + len, 4096-len, "\"%s\"", escape_string_json(sd_value));
    }

    len += snprintf(sd_buffer + len, 4096-len, "}");
    return sd_buffer;
}


void add_data_to_buffer(char *buffer); 

static void logmsg1(struct buf_msg *buffer)
{
    char out_buffer[32768];
    int len = 0;
	static int i = 0;

    len += snprintf(out_buffer, 32768 - len, "{\"hostname1\":\"%s\",", escape_string_json(buffer->hostname));
    len += snprintf(out_buffer + len, 32768 - len, "\"app_name\":\"%s\",", escape_string_json(buffer->app_name ? buffer->app_name : "nil"));
    len += snprintf(out_buffer + len, 32768 - len, "\"proc_id\":\"%s\",", escape_string_json(buffer->proc_id ? buffer->proc_id  : "nil"));
    len += snprintf(out_buffer + len, 32768, "\"msg_id\":\"%s\",", escape_string_json( buffer->msgid ? buffer->msgid    : "nil"));
    len += snprintf(out_buffer + len, 32768, "\"sd\":%s,", sd_to_json( buffer ));
    len += snprintf(out_buffer + len, 32768, "\"message\":\"%s\"", escape_string_json( buffer->msg ? buffer->msg   : "nil"));
	len += snprintf(out_buffer + len, 32768, "}\n");

    printf(out_buffer);
    if (i++ > 20) {
		fflush(stdout); 
		i = 0;
	}
   // printf("Adding data to the buffer");
	//add_data_to_buffer(out_buffer);
	//printf("Added data to the buffer");
}

/*
 * Trims the application name ("TAG" in RFC 3164 terminology) and
 * process ID from a message if present.
 */
static void
parsemsg_rfc3164_app_name_procid(char **msg, char **app_name, char **procid)
{
	char *m, *app_name_begin, *procid_begin;
	size_t app_name_length, procid_length;

	m = *msg;

	/* Application name. */
	app_name_begin = m;
	app_name_length = strspn(m,
	    "abcdefghijklmnopqrstuvwxyz"
	    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	    "0123456789"
	    "_-");
	if (app_name_length == 0)
		goto bad;
	m += app_name_length;

	/* Process identifier (optional). */
	if (*m == '[') {
		procid_begin = ++m;
		procid_length = strspn(m, "0123456789");
		if (procid_length == 0)
			goto bad;
		m += procid_length;
		if (*m++ != ']')
			goto bad;
	} else {
		procid_begin = NULL;
		procid_length = 0;
	}

	/* Separator. */
	if (m[0] != ':' || m[1] != ' ')
		goto bad;

	/* Split strings from input. */
	app_name_begin[app_name_length] = '\0';
	if (procid_begin != 0)
		procid_begin[procid_length] = '\0';

	*msg = m + 2;
	*app_name = app_name_begin;
	*procid = procid_begin;
	return;
bad:
	*app_name = NULL;
	*procid = NULL;
}
/*
 * Parses a syslog message according to RFC 3164, assuming that PRI
 * (i.e., "<%d>") has already been parsed by parsemsg(). The parsed
 * result is passed to logmsg().
 */
static void
parsemsg_rfc3164(int pri, char *msg)
{
	//printf("Parsing as rfc3164\n");
	struct logtime timestamp_remote = { 0 };
	struct buf_msg buffer;
	struct tm tm_parsed;
	size_t i, msglen;
	char line[MAXLINE + 1];

	memset(&buffer, 0, sizeof(buffer));
	buffer.recvhost = "127.0.0.2";
	buffer.pri = pri;
	buffer.msg = line;

	/*
	 * Parse the TIMESTAMP provided by the remote side. If none is
	 * found, assume this is not an RFC 3164 formatted message,
	 * only containing a TAG and a MSG.
	 */
	if (strptime(msg, RFC3164_DATEFMT, &tm_parsed) ==
	    msg + RFC3164_DATELEN && msg[RFC3164_DATELEN] == ' ') {

		msg += RFC3164_DATELEN + 1;

		// if (!RemoteAddDate) {
		// 	struct timeval tv;
		// 	time_t t_remote;
		// 	struct tm tm_now;
		// 	int year;

		// 	if (gettimeofday(&tv, NULL) == -1) {
		// 		tv.tv_sec  = time(NULL);
		// 		tv.tv_usec = 0;
		// 	}

		// 	/*
		// 	 * As the timestamp does not contain the year
		// 	 * number, daylight saving time information, nor
		// 	 * a time zone, attempt to infer it. Due to
		// 	 * clock skews, the timestamp may even be part
		// 	 * of the next year. Use the last year for which
		// 	 * the timestamp is at most one week in the
		// 	 * future.
		// 	 *
		// 	 * This loop can only run for at most three
		// 	 * iterations before terminating.
		// 	 */
		// 	localtime_r(&tv.tv_sec, &tm_now);
		// 	for (year = tm_now.tm_year + 1;; --year) {
		// 		if (year < tm_now.tm_year - 1)
		// 			break;
		// 		timestamp_remote.tm = tm_parsed;
		// 		timestamp_remote.tm.tm_year = year;
		// 		timestamp_remote.tm.tm_isdst = -1;
		// 		t_remote = mktime(&timestamp_remote.tm);
		// 		if ((t_remote != (time_t)-1) &&
		// 		    (t_remote - tv.tv_sec) < 7 * 24 * 60 * 60)
		// 			break;
		// 	}
		// 	buffer.timestamp = timestamp_remote;
		// 	buffer.timestamp.usec = tv.tv_usec;
		// }
	}

	/*
	 * A single space character MUST also follow the HOSTNAME field.
	 */
	msglen = strlen(msg);
	for (i = 0; i < MIN(MAXHOSTNAMELEN, msglen); i++) {
		if (msg[i] == ' ') {
			// if (RemoteHostname) {
			// 	msg[i] = '\0';
			// 	buffer.hostname = msg;
			// }
			msg += i + 1;
			break;
		}
		/*
		 * Support non RFC compliant messages, without hostname.
		 */
		if (msg[i] == ':')
			break;
	}

	if (i == MIN(MAXHOSTNAMELEN, msglen)) {
		logit("Invalid HOSTNAME: %s\n", msg);
		return;
	}

	if (buffer.hostname)
		buffer.hostname = "<UNKNOWN>";

	/* Remove the TAG, if present. */
	parsemsg_rfc3164_app_name_procid(&msg, &buffer.app_name, &buffer.proc_id);
	parsemsg_remove_unsafe_characters(msg, line, sizeof(line));
	logmsg1(&buffer);
}

static void parsemsg_rfc5424(int pri, char *msg)
{
	//printf("Parsing as rfc5424\n");
	fflush(stdout);
	const struct logtime *timestamp = NULL;
	struct logtime timestamp_remote;
	struct buf_msg buffer;
	const char *omsg;
	char line[MAXLINE + 1];
    
	memset(&buffer, 0, sizeof(buffer));

	buffer.recvhost = NULL;
	buffer.pri = pri;
	buffer.msg = line;

#define	FAIL_IF(field, expr) do {					\
	if (expr) {							\
		logit("Failed to parse " field ": %s\n",	\
		       omsg);					\
		return;							\
	}								\
} while (0)
#define	PARSE_CHAR(field, sep) do {					\
	FAIL_IF(field, *msg != sep);					\
	++msg;								\
} while (0)
#define	IF_NOT_NILVALUE(var)						\
	if (msg[0] == '-' && msg[1] == ' ') {				\
		msg += 2;						\
		var = NULL;						\
	} else if (msg[0] == '-' && msg[1] == '\0') {			\
		++msg;							\
		var = NULL;						\
	} else

	omsg = msg;
	IF_NOT_NILVALUE(timestamp) {
		/* Parse RFC 3339-like timestamp. */
#define	PARSE_NUMBER(dest, length, min, max) do {			\
	int i, v;							\
									\
	v = 0;								\
	for (i = 0; i < length; ++i) {					\
		FAIL_IF("TIMESTAMP", *msg < '0' || *msg > '9');		\
		v = v * 10 + *msg++ - '0';				\
	}								\
	FAIL_IF("TIMESTAMP", v < min || v > max);			\
	dest = v;							\
} while (0)
		/* Date and time. */
		memset(&timestamp_remote, 0, sizeof(timestamp_remote));
		PARSE_NUMBER(timestamp_remote.tm.tm_year, 4, 0, 9999);
		timestamp_remote.tm.tm_year -= 1900;
		PARSE_CHAR("TIMESTAMP", '-');
		PARSE_NUMBER(timestamp_remote.tm.tm_mon, 2, 1, 12);
		--timestamp_remote.tm.tm_mon;
		PARSE_CHAR("TIMESTAMP", '-');
		PARSE_NUMBER(timestamp_remote.tm.tm_mday, 2, 1, 31);
		PARSE_CHAR("TIMESTAMP", 'T');
		PARSE_NUMBER(timestamp_remote.tm.tm_hour, 2, 0, 23);
		PARSE_CHAR("TIMESTAMP", ':');
		PARSE_NUMBER(timestamp_remote.tm.tm_min, 2, 0, 59);
		PARSE_CHAR("TIMESTAMP", ':');
		PARSE_NUMBER(timestamp_remote.tm.tm_sec, 2, 0, 59);
		/* Perform normalization. */
		//timegm(&timestamp_remote.tm);
		/* Optional: fractional seconds. */
		if (msg[0] == '.' && msg[1] >= '0' && msg[1] <= '9') {
			int i;

			++msg;
			for (i = 100000; i != 0; i /= 10) {
				if (*msg < '0' || *msg > '9')
					break;
				timestamp_remote.usec += (*msg++ - '0') * i;
			}
		}
		/* Timezone. */
		if (*msg == 'Z') {
			/* UTC. */
			++msg;
		} else {
			int sign, tz_hour, tz_min;

			/* Local time zone offset. */
			FAIL_IF("TIMESTAMP", *msg != '-' && *msg != '+');
			sign = *msg++ == '-' ? -1 : 1;
			PARSE_NUMBER(tz_hour, 2, 0, 23);
			PARSE_CHAR("TIMESTAMP", ':');
			PARSE_NUMBER(tz_min, 2, 0, 59);
		//	timestamp_remote.tm.tm_gmtoff =
		//	    sign * (tz_hour * 3600 + tz_min * 60);
		}
#undef PARSE_NUMBER
		PARSE_CHAR("TIMESTAMP", ' ');
//		if (!RemoteAddDate)
//			timestamp = &timestamp_remote;
		}

	if (timestamp)
		buffer.timestamp = *timestamp;

	/* String fields part of the HEADER. */
#define	PARSE_STRING(field, var)					\
	IF_NOT_NILVALUE(var) {						\
		var = msg;						\
		while (*msg >= '!' && *msg <= '~')			\
			++msg;						\
		FAIL_IF(field, var == msg);				\
		PARSE_CHAR(field, ' ');					\
		msg[-1] = '\0';						\
	}
	PARSE_STRING("HOSTNAME", buffer.hostname);
	//if (buffer.hostname == NULL || !RemoteHostname)
	//	buffer.hostname = (char *)from;
	PARSE_STRING("APP-NAME", buffer.app_name);
	PARSE_STRING("PROCID", buffer.proc_id);
	PARSE_STRING("MSGID", buffer.msgid);
#undef PARSE_STRING

	/* Structured data. */
#define	PARSE_SD_NAME() do {						\
	const char *start;						\
									\
	start = msg;							\
	while (*msg && *msg >= '!' && *msg <= '~' && *msg != '=' &&	\
	    *msg != ']' && *msg != '"')					\
		++msg;							\
	FAIL_IF("STRUCTURED-NAME", start == msg);			\
} while (0)


	IF_NOT_NILVALUE(buffer.sd) {
		buffer.sd = msg;
		/* SD-ELEMENT. */
		while (*msg && *msg == '[') {
			++msg;
			/* SD-ID. */
			PARSE_SD_NAME();
			/* SD-PARAM. */
			while (*msg && *msg == ' ') {
				++msg;
				/* PARAM-NAME. */
                buffer.sd_pairs[buffer.sd_count].value = 0;
                buffer.sd_pairs[buffer.sd_count].name = msg;
				PARSE_SD_NAME();
                buffer.sd_pairs[buffer.sd_count].name_len = msg - buffer.sd_pairs[buffer.sd_count].name;
				PARSE_CHAR("STRUCTURED-NAME", '=');
				PARSE_CHAR("STRUCTURED-NAME", '"');
                buffer.sd_pairs[buffer.sd_count].value = msg;
				while (*msg && *msg != '"') {
					FAIL_IF("STRUCTURED-NAME",
					    *msg == '\0');
					if (*msg++ == '\\') {
						FAIL_IF("STRUCTURED-NAME",
						    *msg == '\0');
						++msg;
					}
				}
                buffer.sd_pairs[buffer.sd_count].value_len = msg - buffer.sd_pairs[buffer.sd_count].value;
                buffer.sd_count++;
				++msg;
			}
			PARSE_CHAR("STRUCTURED-NAME", ']');
		}
		PARSE_CHAR("STRUCTURED-NAME", ' ');
		msg[-1] = '\0';
	}
#undef PARSE_SD_NAME

#undef FAIL_IF
#undef PARSE_CHAR
#undef IF_NOT_NILVALUE
    parsemsg_remove_unsafe_characters(msg, line, sizeof(line));
//    buffer.msg = msg;
	logmsg1(&buffer);
}


void parsemsg(char *msg)
{
	char *q;
	long n;
	size_t i;
	int pri;

	/* Parse PRI. */
	if (msg[0] != '<' || !isdigit(msg[1])) {
		logit("Invalid PRI from :%s ", msg);
		return;
	}
	for (i = 2; i <= 4; i++) {
		if (msg[i] == '>')
			break;
		if (!isdigit(msg[i])) {
			logit("Invalid PRI header");
			return;
		}
	}
	if (msg[i] != '>') {
		logit("Invalid PRI header");
		return;
	}
	errno = 0;
	n = strtol(msg + 1, &q, 10);
	if (errno != 0 || *q != msg[i] || n < 0 || n >= INT_MAX) {
		logit("Invalid PRI %ld: %s\n",
		      n,strerror(errno));
		return;
	}
	pri = n;
	if (pri &~ (LOG_FACMASK|LOG_PRIMASK))
		pri = DEFUPRI;

	// /*
	//  * Don't allow users to log kernel messages.
	//  * NOTE: since LOG_KERN == 0 this will also match
	//  *       messages with no facility specified.
	//  */
	// if ((pri & LOG_FACMASK) == LOG_KERN && 0)
	// 	pri = LOG_MAKEPRI(LOG_USER, LOG_PRI(pri));

	/*
	 * Message looks OK, update current time and log it
	 */
	//timer_update();

	/* Parse VERSION. */
	msg += i + 1;
	if (msg[0] == '1' && msg[1] == ' ')
		parsemsg_rfc5424(pri, msg + 2);
        //printf("Message parsed as rfc 5424\n");
	else
        //printf("Message parsed as rfc 3164\n");
		parsemsg_rfc3164(pri, msg);
}