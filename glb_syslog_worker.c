
/* * Header file for the parser object
 *
 * File begun on 2007-08-06 by RGerhards (extracted from syslogd.c, which
 * was under BSD license at the time of rsyslog fork)
 *
 * Copyright 2007-2018 Adiscon GmbH.
 *
 * This file is formed on part of rsyslog suite.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _GNU_SOURCE

#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include "parser.h"
#include "params.h"
#include "socket.h"
#include "common.h"


static void process_udp(const int sock)
{
	struct sockaddr_in server_sin;
	socklen_t server_sz = sizeof(server_sin);
	char buf[4096];
	int msg_size;

	while (-1 != (msg_size = recv(sock, &buf, sizeof(buf), 0))) 
		parsemsg(buf);	

}

#define DEFAULT_PIPE_SIZE 65536

static size_t get_max_pipe_buff_size() 
{
    long max_pipe_size = 0L;

    FILE* fp = NULL;
    if ( (fp = fopen("/proc/sys/fs/pipe-max-size", "r" )) == NULL ) {
     //   LOG_WRN("Couldn't get max worker pipe size, cannot open proc file, will use 65k bytes as a safe default. Warn: might reduce performance on heavy loaded systems");
        return DEFAULT_PIPE_SIZE;      /* Can't open, default will work on on most systems */
    }
    
    if ( fscanf( fp, "%ld", &max_pipe_size ) != 1 )
    {
        fclose( fp );
      //  LOG_WRN("Couldn't get max worker pipe size, cannot read proc file, will use 65k bytes as a safe default. Warn: might reduce performance on heavy loaded systems");
        return DEFAULT_PIPE_SIZE;      /* Can't read? */
    }
    
    fclose( fp );
    return (size_t)max_pipe_size;
}

int main(int argc, char **argv)
{
	int sock;
	settings_t settings;

	if (-1  == init_params_listen(&settings, argc, argv) &&
		-1  == init_params_env(&settings)) {
		printf("Failed to read command line and ENV options, exiting\n");
		exit(EXIT_FAILURE);
	}

	init_socket(&sock);
	bind_socket(&sock, &settings);

	process_udp(sock);
	
	int max_pipe_buff_size = get_max_pipe_buff_size();
	fcntl(fileno(stdout), F_SETPIPE_SZ, max_pipe_buff_size);

	close(sock);
	return 0;
}
