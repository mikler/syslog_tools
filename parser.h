/* * Header file for the parser object
 *
 * File begun on 2007-08-06 by RGerhards (extracted from syslogd.c, which
 * was under BSD license at the time of rsyslog fork)
 *
 * Copyright 2007-2018 Adiscon GmbH.
 *
 * This file is formed on part of rsyslog suite.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef PARSER_H
#define PARSER_H

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>		/* struct addrinfo */
#include <string.h>
#ifdef __linux__
#include <sys/klog.h>
#endif
#include <sys/param.h>		/* MAXHOSTNAMELEN */
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>		/* struct sockaddr_un */
//#include "queue.h"
#include "syslog.h"
#include <time.h>

/* facility codes */
#define	LOG_KERN	(0<<3)	/* kernel messages */
#define	LOG_USER	(1<<3)	/* random user-level messages */
#define	LOG_MAIL	(2<<3)	/* mail system */
#define	LOG_DAEMON	(3<<3)	/* system daemons */
#define	LOG_AUTH	(4<<3)	/* security/authorization messages */
#define	LOG_SYSLOG	(5<<3)	/* messages generated internally by syslogd */
#define	LOG_LPR		(6<<3)	/* line printer subsystem */
#define	LOG_NEWS	(7<<3)	/* network news subsystem */
#define	LOG_UUCP	(8<<3)	/* UUCP subsystem */
#define	LOG_CRON	(9<<3)	/* clock daemon */
#define	LOG_AUTHPRIV	(10<<3)	/* security/authorization messages (private) */
#define	LOG_FTP		(11<<3)	/* ftp daemon */
#define	LOG_NTP		(12<<3)	/* NTP subsystem */
#define	LOG_SECURITY	(13<<3)	/* Log audit, for audit trails */
#define	LOG_CONSOLE	(14<<3)	/* Log alert */
#define	LOG_CRON_SOL	(15<<3)	/* clock daemon (Solaris) */
#define	LOG_LOCAL0	(16<<3)	/* reserved for local use */
#define	LOG_LOCAL1	(17<<3)	/* reserved for local use */
#define	LOG_LOCAL2	(18<<3)	/* reserved for local use */
#define	LOG_LOCAL3	(19<<3)	/* reserved for local use */
#define	LOG_LOCAL4	(20<<3)	/* reserved for local use */
#define	LOG_LOCAL5	(21<<3)	/* reserved for local use */
#define	LOG_LOCAL6	(22<<3)	/* reserved for local use */
#define	LOG_LOCAL7	(23<<3)	/* reserved for local use */

#define	LOG_NFACILITIES	24	/* current number of facilities */
#define	LOG_FACMASK	0x03f8	/* mask to extract facility part */
				/* facility of pri */
#define	LOG_FAC(p)	(((p) & LOG_FACMASK) >> 3)

#define	LOG_EMERG	0	/* system is unusable */
#define	LOG_ALERT	1	/* action must be taken immediately */
#define	LOG_CRIT	2	/* critical conditions */
#define	LOG_ERR		3	/* error conditions */
#define	LOG_WARN	4	/* warning conditions, alias */
#define	LOG_WARNING	4	/* warning conditions */
#define	LOG_NOTICE	5	/* normal but significant condition */
#define	LOG_INFO	6	/* informational */
#define	LOG_DEBUG	7	/* debug-level messages */

#define	LOG_PRIMASK	0x07	/* mask to extract priority part (internal) */
				/* extract priority */
#define	LOG_PRI(p)	((p) & LOG_PRIMASK)
#define	LOG_MAKEPRI(fac, pri)	((fac) | (pri))


#define MAXLINE        2048            /* maximum line length */
#define MAXSVLINE      MAXLINE         /* maximum saved line length */
#define DEFUPRI        (LOG_USER | LOG_NOTICE)
#define DEFSPRI        (LOG_KERN | LOG_CRIT)
#define TIMERINTVL     30              /* interval for checking flush/nslookup */
#define RCVBUF_MINSIZE (80 * MAXLINE)  /* minimum size of dgram rcv buffer */


/* Timestamps of log entries. */
struct logtime {
	struct tm       tm;
	suseconds_t     usec;
};

typedef struct {
    char *name;
    int name_len;
    char *value;
    int value_len;
} sd_pair_t;

/* message buffer container used for processing, formatting, and queueing */
struct buf_msg {
	int	 	 pri;
	char		 pribuf[8];
	int	 	 flags;
	struct logtime	 timestamp;
	char		 timebuf[33];
	char		*recvhost;
	char		*hostname;
	char		*app_name;
	char		*proc_id;
	char		*msgid;
	char		*sd;	       /* structured data */
	char		*msg;	       /* message content */
	int			sd_count;
	sd_pair_t	sd_pairs[64];
};

#define INTERNAL_INVPRI 0x00    /* Value to indicate no priority in f_pmask */
#define	INTERNAL_NOPRI	0x10	/* the "no priority" priority */
				/* mark "facility" */
#define INTERNAL_ALLPRI 0xFF   /* Value to indicate all priorities in f_pmask */
#define	INTERNAL_MARK	LOG_MAKEPRI(LOG_NFACILITIES << 3, 0)



void parsemsg(char *msg);
#endif