/* * Header file for the parser object
 *
 * File begun on 2007-08-06 by RGerhards (extracted from syslogd.c, which
 * was under BSD license at the time of rsyslog fork)
 *
 * Copyright 2007-2018 Adiscon GmbH.
 *
 * This file is formed on part of rsyslog suite.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "common.h"
#include <getopt.h>

void parse_ip_port(struct sockaddr_in *server, char *option){
	char *default_ip = "127.0.0.1";
	char new_ipaddr[20] = {0};
	char *ipaddr;
	
	unsigned short port = 514;

	char* c_idx = strstr(option, ":");
	
	if (NULL == c_idx) { 			//127.0.0.1
		ipaddr = option;	
	} else if (c_idx == option) { 	//:514
		ipaddr = default_ip;
		port = strtol(c_idx+1, 0, 10);
	} else { 						//127.0.0.1:514
		port = strtol(c_idx+1, 0, 10);
		strncpy(new_ipaddr, option, c_idx - option);
		ipaddr = new_ipaddr;
	}

	server->sin_family = AF_INET;
    server->sin_addr.s_addr = inet_addr(ipaddr);
    server->sin_port = htons(port);

}

#define MAX_PARAM_LEN 4096

int init_params_env(settings_t *settings) {
	char *param;
	char fw_copy[MAX_PARAM_LEN]={0};
	int f = 0;

	if ( NULL == (param = getenv("LISTEN")))
		return -1;

	parse_ip_port(&settings->listen, param); 


	if ( NULL == (param = getenv("FORWARD")))
		return -1;

	strncpy(fw_copy, param, MAX_PARAM_LEN);
	
	char *fwd_ip_port;

	fwd_ip_port = strtok(fw_copy, ";");
    
	while(fwd_ip_port) {
        parse_ip_port(&settings->fwd_socket[f].sockaddr, fwd_ip_port);
		f++;
		settings->total_servers = f;
		fwd_ip_port = strtok(NULL, ";");

		if (f >= MAX_FWD) {
			printf("Only %d targets is allowed\n", MAX_FWD);
			exit(-1);
		}

    }

	if (0 == f) 
		return -1;
	
	return 0;
}

int init_params(settings_t *settings, int argc, char **argv) {
	int c, opt_idx=0;
	int f = 0;
	int listen_set = 0;

	static struct option long_options[] =
	{
    	{"listen", required_argument, NULL, 'l'},
    	{"forward", required_argument, NULL, 'f'},
    	{NULL, 0, NULL, 0}
	};
	
	while(1) {
		if ( -1 == (c = getopt_long_only(argc, argv,"", long_options, &opt_idx)))
			break;
		
		switch (c) {

			case 'l':
				parse_ip_port(&settings->listen, optarg);
				listen_set = 1;
				break;

			case 'f':
				parse_ip_port(&settings->fwd_socket[f].sockaddr, optarg);
				f++;
				settings->total_servers = f;

				break;
			case '?':
				printf("Use: --listen=0.0.0.0:514 --forward=1.2.4.5:515 --forward=1.2.5.4\n Default port is 514");
				exit(-1);
			default:
        	  	abort();
        }
		if (f >= MAX_FWD) {
			printf("Only %d targets is allowed\n", MAX_FWD);
			exit(-1);
		}
	}
	if (0 == listen_set || 0 == f) {
		printf("Not enough command line params");
		return -1;
	}

	return 0;
}

int init_params_listen(settings_t *settings, int argc, char **argv) {
	int c, opt_idx=0;
	int listen_set = 0;

	static struct option long_options[] =
	{
    	{"listen", required_argument, NULL, 'l'},
    	{NULL, 0, NULL, 0}
	};
	
	while(1) {
		if ( -1 == (c = getopt_long_only(argc, argv,"", long_options, &opt_idx)))
			break;
		
		switch (c) {

			case 'l':
				parse_ip_port(&settings->listen, optarg);
				listen_set = 1;
				break;

			case '?':
				printf("Use: --listen=0.0.0.0:514 or set env LISTEN var ");
				exit(-1);
			default:
        	  	abort();
        }
	}
	if (0 == listen_set) {
		printf("Not enough command line params");
		return -1;
	}

	return 0;
}
