/* * Header file for the parser object
 *
 * File begun on 2007-08-06 by RGerhards (extracted from syslogd.c, which
 * was under BSD license at the time of rsyslog fork)
 *
 * Copyright 2007-2018 Adiscon GmbH.
 *
 * This file is formed on part of rsyslog suite.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h> 
#include <unistd.h> 
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include "common.h"
#include "params.h"
#include "socket.h"

static void replicate_udp(const int sock, settings_t *settings)
{

	char buf[4096];
	int msg_size = 0;
	
	while(-1 !=(msg_size = recv(sock, buf, 4096, 0)))
 	{	
		buf[msg_size] = '\0';
		for (int i = 0; i < settings->total_servers; i++) {
			sendto(sock, buf, msg_size, 0, (struct sockaddr *)&settings->fwd_socket[i].sockaddr, sizeof(struct sockaddr_in));
		}
	}
}


int main(int argc, char **argv)
{
	int sock;
	settings_t settings = {0};
   
	if (-1  == init_params(&settings, argc, argv) &&
		-1  == init_params_env(&settings)) {
		printf("Failed to read command line and ENV options, exiting\n");
		exit(EXIT_FAILURE);
	}

	init_socket(&sock);
	bind_socket(&sock, &settings);
	replicate_udp(sock, &settings);

	close(sock);
    
	return 0;
}