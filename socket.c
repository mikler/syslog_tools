/* * Header file for the parser object
 *
 * File begun on 2007-08-06 by RGerhards (extracted from syslogd.c, which
 * was under BSD license at the time of rsyslog fork)
 *
 * Copyright 2007-2018 Adiscon GmbH.
 *
 * This file is formed on part of rsyslog suite.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *       -or-
 *       see COPYING.ASL20 in the source distribution
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <arpa/inet.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include <unistd.h> 
#include <strings.h>

void init_socket(int *sock) {
	*sock = socket(AF_INET, SOCK_DGRAM, 0);

	int flags = fcntl(*sock, F_GETFL);
	int size = 100 * 1024* 1024;

	if (-1 == setsockopt(*sock, SOL_SOCKET, SO_RCVBUF, &size, sizeof(int))) {
		printf("Couldn't send max socket buffer size to %d bytes", 1024*1024);
		exit(-1);
	}
	if (-1 == setsockopt(*sock, SOL_SOCKET, SO_SNDBUF, &size, sizeof(int))) {
		printf("Couldn't send max socket buffer size to %d bytes", 1024*1024);
		exit(-1);
	}
}

void bind_socket(int *sock, settings_t *settings){

	if (bind(*sock, (struct sockaddr *) &settings->listen, sizeof(struct sockaddr))) {
		perror("bind()");
		exit(EXIT_FAILURE);
	}
}

//void init_forward_sockets(settings_t *settings) {
// 	bzero(settings->fwd_socket,sizeof(forward_t) * MAX_FWD);
// }
